
import Head from 'next/head'
import NavBar from '../components/NavBar'
import Image from 'next/image'
import menuHome from '../components/Menu'
import Contato from '../components/Contato'
import Rodape from '../components/Rodape'
import Link from 'next/link'
import Home from '../components/Home'


// const Header = () => (
//   <div class="bg-black w-full grid grid-cols-3  justify-items-center shadow-2xl">

//     <div class="col-span-1">
//       <a href="#" class="">
//         <img class="" src="/logo.png" width={120} />
//       </a>
//     </div>

//     <nav class="mt-8 col-start-2 col-span-2">
//       <ul class="text-white text-semibold flex flex-start space-x-2 text-xl">
//         <li class="hover:text-orange hover:border-orange hover:border-2"><a href="#">Início</a></li>
//         <li class="hover:text-orange"><a href="#">Orçamento</a></li>
//         <li class="hover:text-orange"><a href="#">Profissionais</a></li>
//         <li class="hover:text-orange"><a href="#">Quem somos</a></li>
//         <li class="hover:text-orange"><a href="#">Blog</a></li>
//       </ul>
//     </nav>

//     <div class="mt-8 grid grid-cols-2 justify-items-end col-end-9 gap-4">
//       <Link href="/login" class="">
//         <button class="flex-shrink-0 text-center transition ease-out duration-500 hover:bg-white hover:bg-opacity-10 hover:text-orange rounded-md font-semibold text-xl text-gray-100 p-2">Entrar</button>

//       </Link>
//       <Link href="/cadastro" class="flex">
//         <button class="flex-shrink-0 text-center transition ease-out duration-500 bg-orange hover:bg-opacity-10 hover:text-orange rounded-md font-semibold text-xl text-gray-100 p-2">Cadastrar-se como profissional</button>
//       </Link>
//       <Link href="/dashboard/inicio" class="flex">
//         <button class="flex-shrink-0 text-center transition ease-out duration-500 bg-orange hover:bg-opacity-10 hover:text-orange rounded-md font-semibold text-xl text-gray-100 p-2">DASHBOARD</button>
//       </Link>
//     </div>


//   </div>
// )

function Hero() {
  return (
    <>
      <Image
        src="/hero.png"
        alt="Picture of the author"
        width={500}
        height={500}
      />
    </>
  )
}

function Index() {


  return (
    <div class="w-screen h-screen">
      <Head>
        <title>Keroservice</title>
        <link rel="icon" href="/keroservice.svg" />
      </Head>
      <Home />
    </div>




  )
}


export default Index