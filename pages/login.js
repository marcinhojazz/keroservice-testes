import React from 'react'
import Rodape from '../components/Rodape'
import Link from 'next/link'
import Navv from '../components/Navv'


const Contato = () => {
  return (

    <div>
      <Navv />


      <main>
        <section class="text-gray-600 body-font flex justify-center">
          <div class="container px-5 py-24 mx-auto">
            <div class="flex flex-col text-center w-full mb-12">
              <h1 class="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">Login</h1>
            </div>

            <div class="lg:w-1/4 md:w-2/3 mx-auto">
              <div class="flex flex-wrap -m-2">

                <div class="p-2 w-full">
                  <div class="relative">
                    <label htmlFor="name" class="leading-7 text-sm text-gray-600">Nome</label>
                    <input type="text" id="name" name="name" class="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-orange focus:bg-white focus:ring-2 focus:ring-orange-light text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out" />
                  </div>
                </div>
                <div class="p-2 w-full">
                  <div class="relative">
                    <label htmlFor="password" class="leading-7 text-sm text-gray-600">Email</label>
                    <input type="password" id="password" name="password" class="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-orange focus:bg-white focus:ring-2 focus:ring-orange-light text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out" />
                  </div>
                </div>
                <div class="p-2 w-full">
                  <Link href="/dashboard/inicio">
                    <div class="relative">
                      <button class="hover:bg-orange-dark w-full bg-orange rounded border border-gray-300 focus:border-orange focus:bg-black focus:ring-2 focus:ring-orange-light text-base outline-none text-gray-100 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out">Entrar</button>
                    </div>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </section >

      </main>
      <Rodape />
    </div>
  )
}

export default Contato
