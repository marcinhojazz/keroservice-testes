import Link from 'next/link'
import React from 'react'
import Navv from '../components/Navv'
import Rodape from '../components/Rodape'




function Cadastro() {
  return (

    <div>

      <Navv />

      <main>
        <section class="text-gray-600 body-font flex justify-center">
          <div class="container px-5 py-24 mx-auto">
            <div class="flex flex-col text-center w-full mb-12">
              <h1 class="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">Cadastro</h1>
              <p class=" text-gray-500">Faça seu cadastro e receba contato de clientes por 30 dias gratuito.</p>
            </div>

            <div class="lg:w-1/4 md:w-2/3 mx-auto">
              <div class="flex flex-wrap -m-2">


                {/* NOME */}
                <div class="p-2 w-full">
                  <div class="relative">
                    <label htmlFor="name" class="leading-7 text-sm text-gray-600">Nome Completo:</label>
                    <input type="text" id="name" name="name" class="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-orange focus:bg-white focus:ring-2 focus:ring-orange-light text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out" />
                  </div>
                </div>
                {/* EMAIL */}
                <div class="p-2 w-full">
                  <div class="relative">
                    <label htmlFor="email" class="leading-7 text-sm text-gray-600">Email:</label>
                    <input type="text" id="email" name="email" class="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-orange focus:bg-white focus:ring-2 focus:ring-orange-light text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out" />
                  </div>
                </div>
                <div class="p-2 w-full">
                  <div class="relative">
                    <label htmlFor="phone" class="leading-7 text-sm text-gray-600">Celular:</label>
                    <input type="tel" id="phone" name="phone" placeholder="(21) 9999-9999" class="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-orange focus:bg-white focus:ring-2 focus:ring-orange-light text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out" />
                  </div>
                </div>






                <div class="p-2 w-full">
                  <div class="relative">
                    <Link href="/confirmacao">
                      <button class="w-full uppercase font-medium bg-orange rounded border border-gray-300 focus:border-orange focus:ring-2 focus:bg-white  focus:ring-orange-light focus:text-orange text-base outline-none text-gray-100 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out">
                        Cadastrar Serviços
                      </button>
                    </Link>
                  </div>

                </div>

              </div>
            </div>
          </div>
        </section >
      </main>
      <Rodape />


    </div>

  )
}

export default Cadastro
