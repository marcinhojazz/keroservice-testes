import React from 'react'
import Navv from '../components/Navv'


import { useState } from 'react'
import { Switch } from '@headlessui/react'
import Rodape from '../components/Rodape'

function MyToggle() {
  const [enabled, setEnabled] = useState(false)

  return (
    <Switch
      checked={enabled}
      onChange={setEnabled}
      className={`${enabled ? 'bg-orange' : 'bg-black'
        } relative inline-flex items-center h-6 rounded-full w-11`}
    >
      <span className="sr-only">Enable notifications</span>
      <span
        className={`${enabled ? 'translate-x-6' : 'translate-x-1'
          } inline-block w-4 h-4 transform bg-white rounded-full`}
      />
    </Switch>
  )
}

function Orcamento() {

  const categoriaServicos = [
    'Energia Solar',
    'Ar Condicionado',
    'Telecomunicações',
    'Automação'
  ]

  const subcategorias = ['Deslizante', 'Basculante', 'Pivotante', 'Porta Automática', 'Outros']

  const tipoCliente = ['Residencial', 'Empresarial', 'Condominial', 'Governamental', 'Rural']

  const tipoServicos = ['Compra', 'Instalação', 'Manutenção', 'Aluguel',]

  return (
    <div class="w-screen h-screen">
      <Navv />

      <main>
        {/* ESCOLHER CATEGORIA DE SERVIÇO */}
        <div class="mt-12 grid grid-cols-1 justify-items-center gap-4 h-80">
          <div class="">
            <p class="text-orange font-bold text-2xl text-center">Escolha uma categoria de serviço que deseja atender</p>
            <div class="mt-4">

              <ul class="flex justify-between items-center space-x-4">
                {categoriaServicos.map((categoriaServico) => (
                  <li>
                    <button class="border item w-40 flex-grow p-2 bg-orange hover:bg-orange-dark text-gray-100 text-lg font-semibold focus:ring-2 ring-orange-light focus:outline-none focus:bg-black focus:text-orange shadow-lg" key={categoriaServico}>{categoriaServico}</button>
                  </li>
                ))}
              </ul>
              {/* SUBCATEGORIAS */}
              <div class="">
                <div class="p-2 text-orange font-semibold mt-4">SELECIONE ABAIXO AS SUBCATEGORIAS QUE DESEJA ATENDER</div>
                <div class="grid grid-cols-2 font-light">
                  <div>Selecionar</div>
                  <div class="ml-8">Automático</div>
                </div>
                <div class="mt-4">
                  <ul>
                    {subcategorias.map((subcategoria) => (
                      <li key={subcategoria}>
                        <div class="grid grid-cols-2">
                          <div>
                            <div class="space-x-4">
                              <input id="deslizante" class="shadow-lg" id="deslizante2" type="checkbox" class="bg-black text-orange h-4 w-4" />
                              <label htmlFor={subcategoria} key={subcategoria}>{subcategoria}</label>
                            </div>
                          </div>
                        </div>
                      </li>
                    ))}
                  </ul>
                </div>
              </div>

              {/* ESCOLHER CATEGORIA DE CLIENTE */}
              <div class="mt-4">
                <p class="text-orange font-bold text-2xl shadow-lg text-center">
                  Escolha um tipo de serviço que deseja atender?
                </p>

                <ul>
                  {tipoCliente.map((cliente) => (
                    <li key={cliente} class="space-x-4">
                      <input class="bg-black text-orange h-4 w-4" type="checkbox" id={cliente} name={tipoCliente} value={cliente} />
                      <label for={cliente}>{cliente}</label>
                    </li>
                  ))}
                </ul>
              </div>
              {/* ESCOLHER CATEGORIA DE CLIENTE */}

              {/* ESCOLHER TIPO DE SERVIÇO */}
              <div class="mt-4">
                <p class="text-orange font-bold text-2xl shadow-lg text-center">
                  Escolha uma categoria de cliente que deseja atender?
                </p>

                <ul>
                  {tipoServicos.map((tipoServico) => (
                    <li key={tipoServico} class="space-x-4">
                      <input class="bg-black text-orange h-4 w-4" type="checkbox" id={tipoServico} name={tipoServicos} value={tipoServico} />
                      <label for={tipoServico}>{tipoServico}</label>
                    </li>
                  ))}
                </ul>
              </div>
              {/* ESCOLHER TIPO DE SERVIÇO */}



              {/* ENVIAR DOC */}
              <div class="mt-4">
                <p class="text-orange font-bold text-2xl shadow-lg text-start">Enviar documentos</p>
                <div class="mt-4 space-y-2">
                  <div>
                    <input multiple type="file" accept="..png, .jpg, .jpeg, .pdf" />
                  </div>
                </div>
              </div>
              {/* ENVIAR DOC */}

              {/* OBSERVAÇÃO */}
              <div class="grid grid-cols-1 justify-center mt-4">
                <div>
                  <textarea class="w-full min-h-full max-h-28" name="observacao" id="" cols="30" rows="10">dasdasda</textarea>
                </div>
              </div>
              {/* OBSERVAÇÃO */}

              {/* OBSERVAÇÃO */}
              <div class="grid grid-cols-2 justify-items-center my-12">
                <div class="">
                  <button class="border item w-40 flex-grow p-2 bg-orange hover:bg-opacity-0 hover:text-orange hover:border hover:border-orange text-gray-100 text-lg font-semibold focus:ring-2 ring-orange-light focus:outline-none focus:bg-black focus:text-orange shadow-lg">Cancelar</button>
                </div>
                <div class="">
                  <button class="border item w-40 flex-grow p-2 bg-orange hover:bg-orange-dark text-gray-100 text-lg font-semibold focus:ring-2 ring-orange focus:outline-none focus:bg-black focus:text-orange shadow-lg">Avançar</button>
                </div>
              </div>
              {/* OBSERVAÇÃO */}




            </div>
          </div>
        </div>
      </main>

    </div>
  )
}

export default Orcamento

{/* <button class="bg-orange rounded-lg p-2 font-semibold text-white hover:bg-gray-300 hover:text-orange">Energia Solar</button>,
<button class="bg-orange rounded-lg p-2 font-semibold text-white hover:bg-gray-300 hover:text-orange">Energia Solar</button>,
<button class="bg-orange rounded-lg p-2 font-semibold text-white hover:bg-gray-300 hover:text-orange">Energia Solar</button>,
<button class="bg-orange rounded-lg p-2 font-semibold text-white hover:bg-gray-300 hover:text-orange">Energia Solar</button> */}