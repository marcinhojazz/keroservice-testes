import React from 'react'

const Enviosms = () => {
  return (
    <div class="h-screen bg-black grid grid-cols-1 grid-rows-2 justify-items-center gap-8">
      <div>
        <div class="">

          <img class="border border-12 border-white bg-orange rounded-full" src="/confirm.svg" alt="" />
        </div>
        <p class="text-center text-orange font-semibold">ENVIAMOS UM SMS PARA SEU CELULAR</p>
      </div>
      <div>
        <button class="p-3 font-semibold bg-orange text-gray-200 hover:bg-orange-light hover:text-black hover:scale-150">
          Confirmar código de verificação
        </button>
      </div>

    </div>
  )
}

function Cadastroprofissionais() {
  return (
    <div>
      <div class="grid lg:grid-cols-3 bg-gradient-to-tr from-green-500 to-purple-900">
        <div></div>
        <Enviosms />
        <div></div>
      </div>
    </div>
  )
}

export default Cadastroprofissionais
