const colors = require('tailwindcss/colors')

module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],

  darkMode: false, // or 'media' or 'class'
  theme: {
    gradients: theme => ({
      'blue-green': [theme('colors.blue.500'), theme('colors.green.500')],
      'purple-blue': [theme('colors.purple.500'), theme('colors.blue.500')],
    }),

    fontFamily: {
      'html': ['ROBOTO'],
    },
    button: {

    },
    screens: {
      sm: '480px',
      md: '768px',
      lg: '976px',
      xl: '1440px',
    },
    zIndex: {
      '0': 0,
      '10': 10,
      '20': 20,
      '30': 30,
      '40': 40,
      '50': 50,
      '75': 75,
      '100': 100,
      'auto': 'auto',
    },
    backgroundImage: {
      'hero': "url('/public/hero.png')",
    },
    colors: {

      white: {
        DEFAULT: '#F1F1F1'
      },
      gray: colors.coolGray,
      orange: {
        light: '#FDBA74',
        DEFAULT: '#F97316',
        dark: '#C2410C',
      },
      blue: colors.lighblue,
      red: colors.rose,
      black: {
        light: '#8c8c8c',
        DEFAULT: '#2c2c2c',
        dark: '#000'
      }
    },

    extend: {
      backgroundImage: {
        none: 'none',
        'gradient-to-t': 'linear-gradient(to top, var(--tw-gradient-stops))',
        'gradient-to-tr': 'linear-gradient(to top right, var(--tw-gradient-stops))',
        'gradient-to-r': 'linear-gradient(to right, var(--tw-gradient-stops))',
        'gradient-to-br': 'linear-gradient(to bottom right, var(--tw-gradient-stops))',
        'gradient-to-b': 'linear-gradient(to bottom, var(--tw-gradient-stops))',
        'gradient-to-bl': 'linear-gradient(to bottom left, var(--tw-gradient-stops))',
        'gradient-to-l': 'linear-gradient(to left, var(--tw-gradient-stops))',
        'gradient-to-tl': 'linear-gradient(to top left, var(--tw-gradient-stops))',
      },

      spacing: {
        '128': '32rem',
        '144': '36rem',
      },
      borderRadius: {
        '4xl': '2rem',
      },
      translate: ['motion-safe'],
      animation: {
        none: 'none',
        spin: 'spin 1s linear infinite',
        ping: 'ping 1s cubic-bezier(0, 0, 0.2, 1) infinite',
        pulse: 'pulse 2s cubic-bezier(0.4, 0, 0.6, 1) infinite',
        bounce: 'bounce 1s infinite',



        'spin-slow': 'spin 2s linear infinite',
        'wiggle': 'wiggle 1s ease-in-out infinite'

      },

    },
  },
  variants: {
    extend: {

    },
  },
  plugins: [
    require('@headlessui/react'),
    require('@tailwindcss/forms'),
  ],
  corePlugins: {
    preflight: true,
  }
}
