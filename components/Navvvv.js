import { Menu } from "@headlessui/react";

function MyDropdown() {
  return (
    <Menu>
      <Menu.Button>More</Menu.Button>

      {/*
        By default, this will automatically show/hide when the
        Menu.Button is pressed.
      */}
      <Menu.Items>
        <Menu.Item>{/* ... */}</Menu.Item>
        {/* ... */}
      </Menu.Items>
    </Menu>
  );
}

export default MyDropdown