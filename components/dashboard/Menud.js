import React from 'react'

function Menud() {
  return (
    <div class="header-1 flex flex-col bg-gray-200 min-h-screen">

      <nav class="bg-gray-800 px-4 py-2 flex flex-col lg:flex-row lg:items-center flex-shrink-0">
        <div class="flex justify-between items-center lg:mr-32">
          <span class="text-white text-xl">FWR</span>
          <button class="border border-white px-2 py-1 rounded text-white opacity-50 hover:opacity-75 lg:hidden" id="navbar-toggle">
            <i class="fas fa-bars"></i>
          </button>
        </div>
        <div class="hidden lg:flex flex-grow" id="navbar-collapse">
          <ul class="flex flex-col mt-3 mb-1 lg:flex-row lg:mx-auto lg:mt-0 lg:mb-0">
            <li>
              <a href="#" class="block text-gray-500 hover:text-gray-300 py-2 lg:px-2 lg:mx-2 transition-colors duration-300">Home</a>
            </li>
            <li>
              <a href="#" class="block text-gray-500 hover:text-gray-300 py-2 lg:px-2 lg:mx-2 transition-colors duration-300">About</a>
            </li>
            <li>
              <a href="#" class="block text-gray-500 hover:text-gray-300 py-2 lg:px-2 lg:mx-2 transition-colors duration-300">Features</a>
            </li>
            <li>
              <a href="#" class="block text-gray-500 hover:text-gray-300 py-2 lg:px-2 lg:mx-2 transition-colors duration-300">Contact</a>
            </li>
            <li>
              <a href="#" class="block text-gray-500 hover:text-gray-300 py-2 lg:px-2 lg:mx-2 transition-colors duration-300">Blog</a>
            </li>
          </ul>
          <div class="flex my-3 lg:my-0">
            <button class="bg-transparent hover:bg-white text-white hover:text-gray-900 py-1 px-3 rounded border border-solid border-white mr-2 transition-colors duration-300">
              Sign In
                    </button>
            <button class="bg-indigo-600 hover:bg-indigo-700 text-white py-1 px-3 rounded border border-solid border-indigo-600 hover:border-indigo-700 transition-colors duration-300">
              Sign Up
                    </button>
          </div>
        </div>
      </nav>

      <div class="flex content-center p-4 lg:py-16 lg:px-8 text-center max-w-xl mx-auto my-auto">
        <div class="px-2">
          <span class="fas fa-bookmark w-12 h-12 lg:w-16 lg:h-16 bg-indigo-600 rounded-full text-center text-white text-lg lg:text-2xl pt-4 lg:pt-5"></span>
          <h1 class="text-center text-4xl lg:text-5xl my-3 lg:mt-4">Hello, world!</h1>
          <p class="text-xl">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid autem dolore dolorem ea inventore molestiae nemo neque non, quidem recusandae soluta tenetur! Aut eaque, placeat!</p>
          <button class="bg-indigo-600 hover:bg-indigo-700 text-white py-2 px-4 lg:py-3 lg:px-6 rounded mt-6 lg:mt-12 transition-colors duration-300">Learn more</button>
        </div>
      </div>

    </div>


  )
}

export default Menud

