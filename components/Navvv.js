import { Menu } from "@headlessui/react";

function Navvv() {



  return (
    <Menu>
      <Menu.Button class="bg-black">
        <svg xmlns="http://www.w3.org/2000/svg" class="text-right float-left text-gray-300 hover:text-white h-10 w-10 hover:border-orange border border-orange-dark rounded-md m-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
        </svg>
      </Menu.Button>
      <Menu.Items className="text-center mr-8 sm:bg-text-white text-white font-semibold lg:grid lg:grid-cols-7 sm:grid sm:grid-rows-7 sm:bg-black">
        <Menu.Item>
          {({ active }) => (
            <a
              className={`${active && "sm:hover:bg-orange bg-blue-500"}`}
              href="/account-settings"
            >
              Início
            </a>
          )}
        </Menu.Item>
        <Menu.Item>
          {({ active }) => (
            <a
              className={`${active && "sm:hover:bg-orange hover:text-white bg-blue-500"}`}
              href="/account-settings"
            >
              Orçamento
            </a>
          )}
        </Menu.Item>
        <Menu.Item>
          {({ active }) => (
            <a
              className={`${active && "sm:hover:bg-orange hover:text-white bg-blue-500"}`}
              href="/account-settings"
            >
              Profissionais
            </a>
          )}
        </Menu.Item>
        <Menu.Item>
          {({ active }) => (
            <a
              className={`${active && "sm:hover:bg-orange hover:text-white bg-blue-500"}`}
              href="/account-settings"
            >
              Sobre
            </a>
          )}
        </Menu.Item>
        <Menu.Item>
          {({ active }) => (
            <a
              className={`${active && "sm:hover:bg-orange hover:text-white bg-blue-500"}`}
              href="/account-settings"
            >
              Blog
            </a>
          )}
        </Menu.Item>


        <Menu.Item>
          {({ active }) => (
            <a
              className={`${active && "hover:bg-orange bg-blue-500"}`}
              href="/account-settings"
            >
              <button class="sm:border-opacity-0 sm:bg-transparent bg-transparent hover:bg-white text-white hover:text-gray-900 py-1 px-3 rounded border border-solid border-white hover:border-orange mr-2 transition-colors duration-300">
                Entrar
              </button>
            </a>
          )}
        </Menu.Item>
        <Menu.Item>
          {({ active }) => (
            <a
              className={`${active && "bg-blue-500"}`}
              href="/account-settings"
            >
              <button class="bg-orange hover:bg-opacity-20 text-white py-1 px-3 rounded border border-solid border-indigo-600 hover:border-orange ring-orange transition-colors duration-300">
                Cadastrar-se como profissional
              </button>
            </a>
          )}
        </Menu.Item>

      </Menu.Items>
    </Menu>
  );
}

export default Navvv