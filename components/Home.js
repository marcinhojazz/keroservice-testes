import React, { useState } from 'react'
import Image from 'next/image'
import Rodape from './Rodape'
import Navv from '../components/Navv'
import Link from 'next/link'

function Hero() {
  return (
    <>
      <Image
        src="/hero.png"
        alt="Picture of the author"
        width={500}
        height={500}
      />
    </>
  )
}

function Home() {

  return (

    <div class="header-1 flex flex-col bg-gray-100 min-h-screen">

      <Navv>
      </Navv>


      {/* HERO */}
      <div class="h-screen flex content-center p-4 lg:py-16 lg:px-8 text-left max-w-xl mx-auto my-auto">

        <div class="space-y-4 w-4/5">
          <h1 class="text-5xl font-bold">
            Solicite seu orçamento, um único pedido até 4 orçamentos.
          </h1>
          <Link href="/orcamento">
            <button class="text-3x1 bg-gradient-to-t from-orange to-orange-light hover:from-orange-dark hover:to-orange p-2 font-semibold text-gray-100 rounded">
              Solicitar agora
            </button>
            {/* <button class="bg-indigo-600 hover:bg-indigo-700 text-white py-2 px-4 lg:py-3 lg:px-6 rounded mt-6 lg:mt-12 transition-colors duration-300 bg-orange shadow-lg">
              Solicitar agora
            </button> */}
          </Link>
        </div>
      </div>


      {/* section 2 */}
      <div class="p-4 h-screen grid justify-center">
        <div class="space-y-4">
          <div class="flex">
            <iframe width="500" height="250" src="https://www.youtube.com/embed/qz9k-m5cnP0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
          <Link href="/cadastro">
            <button class="text-3x1 bg-gradient-to-t from-orange to-orange-light hover:from-orange-dark hover:to-orange p-2 font-semibold text-gray-100 rounded">
              Cadastre-se como profissional
          </button>
          </Link>
        </div>
      </div>

      {/* section 2 */}
      <div class="sm:justify-items-center lg:flex lg:mx-12 mt-32 h-auto grid lg:grid-cols-2 justify-center lg:gap-4 p-4">
        <div class="">
          <div class="w-2/3">
            <h1 class="text-3xl font-bold flex text-left">
              Encontre de maneira rápida profissionais que atendam na sua região.
            </h1>
          </div>
          <Link href="/orcamento">
            <button class="text-3x1 bg-gradient-to-t from-orange to-orange-light hover:from-orange-dark hover:to-orange p-2 font-semibold text-gray-100 rounded">
              Solicitar um orçamento
            </button>
          </Link>
        </div>
        <div class="">
          <Hero />
        </div>
      </div>

      {/* ------------------------------------------ */}
      <div class="grid grid-rows-2 gap-8">
        {/* QUEM SOMOS */}
        <div class="grid lg:grid-cols-3 justify-items-center mt-20">
          <div class="mt-24 h-40 w-80">
            <div class="space-y-4">
              <h3 class="font-bold text-3xl">Quem somos</h3>
              <p>Saiba mais dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et</p>
              <a href="#" class="text-orange font-semibold">Saiba mais</a>
            </div>
          </div>
          <div class="mt-24 h-40 w-80">
            <div class="space-y-4">
              <h3 class="font-bold text-3xl">Quem somos</h3>
              <p>Saiba mais dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et</p>
              <a href="#" class="text-orange font-semibold">Saiba mais</a>
            </div>
          </div>
          <div class="mt-24 h-40 w-80">
            <div class="space-y-4">
              <h3 class="font-bold text-3xl">Quem somos</h3>
              <p>Saiba mais dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et</p>
              <a href="#" class="text-orange font-semibold">Saiba mais</a>
            </div>
          </div>
        </div>

        {/* NEW */}
        <div class="p-2 grid sm:grid-rows-2 lg:grid-cols-2 justify-items-center mt-20 sm:gap-12">
          {/* coluna 1 */}
          <div class="group bg-gradient-to-br from-black to-black-dark p-2 rounded-lg w-2/3 h-64 shadow-xl cursor-pointer hover:opacity-90">
            <div class="space-y-4">
              <div class="text-orange text-opacity-75 group-hover:text-orange">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-20 w-20" viewBox="0 0 25 25" fill="currentColor">
                  <path fill-rule="evenodd" d="M6 6V5a3 3 0 013-3h2a3 3 0 013 3v1h2a2 2 0 012 2v3.57A22.952 22.952 0 0110 13a22.95 22.95 0 01-8-1.43V8a2 2 0 012-2h2zm2-1a1 1 0 011-1h2a1 1 0 011 1v1H8V5zm1 5a1 1 0 011-1h.01a1 1 0 110 2H10a1 1 0 01-1-1z" clip-rule="evenodd" />
                  <path d="M2 13.692V16a2 2 0 002 2h12a2 2 0 002-2v-2.308A24.974 24.974 0 0110 15c-2.796 0-5.487-.46-8-1.308z" />
                </svg>
              </div>
              <p class="text-white text-opacity-80 font-bold text-2xl group-hover:text-white">Cadastre-se como profissional</p>
              <div class="float-right">
                <Link href="/cadastro">
                  <a href="#">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-16 w-16 transition duration-500 ease-in-out text-orange hover:text-white transform hover:-translate-y-1 hover:scale-110" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 9l3 3m0 0l-3 3m3-3H8m13 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                  </a>
                </Link>
              </div>
            </div>
          </div>
          {/* coluna 2 */}
          <div class="group bg-gradient-to-br from-black to-black-dark p-2 rounded-lg w-2/3 h-64 shadow-xl cursor-pointer hover:opacity-90">
            <div class="space-y-4">
              <div class="text-orange text-opacity-75 group-hover:text-orange">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-20 w-20" viewBox="0 0 25 25" fill="currentColor">
                  <path fill-rule="evenodd" d="M6 6V5a3 3 0 013-3h2a3 3 0 013 3v1h2a2 2 0 012 2v3.57A22.952 22.952 0 0110 13a22.95 22.95 0 01-8-1.43V8a2 2 0 012-2h2zm2-1a1 1 0 011-1h2a1 1 0 011 1v1H8V5zm1 5a1 1 0 011-1h.01a1 1 0 110 2H10a1 1 0 01-1-1z" clip-rule="evenodd" />
                  <path d="M2 13.692V16a2 2 0 002 2h12a2 2 0 002-2v-2.308A24.974 24.974 0 0110 15c-2.796 0-5.487-.46-8-1.308z" />
                </svg>
              </div>
              <p class="text-white text-opacity-80 font-bold text-2xl group-hover:text-white">Cadastre-se como profissional</p>
              <div class="float-right">
                <Link href="/cadastro">
                  <a href="#">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-16 w-16 transition duration-500 ease-in-out text-orange hover:text-white transform hover:-translate-y-1 hover:scale-110" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 9l3 3m0 0l-3 3m3-3H8m13 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                  </a>
                </Link>
              </div>
            </div>
          </div>



          {/* coluna 2 */}

        </div>
        {/* ------------------------------------------ */}
      </div>

      <div class="mt-12">
        <Rodape />
      </div>
    </div >


  )
}

export default Home
