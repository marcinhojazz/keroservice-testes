import React from 'react'

function Rodape() {
  return (

    <div class="grid grid-cols-1 justify-center bg-gradient-to-br from-black to-black-dark text-white gap-32">

      <div class="mt-12 grid md:grid-cols-3 sm:grid-row-3 justify-items-center">

        <div>
          <h3>Lorem ipsum</h3>
          <div class="mt-2">
            <div>
              <ul>
                <li class="text-orange hover:text-white"><a href="">Políticas de Privacidade</a></li>
                <li class="text-orange hover:text-white"><a href="">Políticas de Privacidade</a></li>
                <li class="text-orange hover:text-white"><a href="">Políticas de Privacidade</a></li>
              </ul>
            </div>
            <input type="text" />
            <button></button>

          </div>

        </div>
        <div class="mt-6">
          <h3>Lorem ipsum</h3>
          <div class="mt-2">
            <ul>
              <li class="text-orange hover:text-white"><a href="">Políticas de Privacidade</a></li>
              <li class="text-orange hover:text-white"><a href="">Políticas de Privacidade</a></li>
              <li class="text-orange hover:text-white"><a href="">Políticas de Privacidade</a></li>
              <li class="text-orange hover:text-white"><a href="">Políticas de Privacidade</a></li>
            </ul>
          </div>

        </div>
        <div class="mt-6">
          <h3>Lorem ipsum</h3>
          <div class="mt-2">
            <ul>
              <li class="text-orange hover:text-white"><a href="">Políticas de Privacidade</a></li>
              <li class="text-orange hover:text-white"><a href="">Políticas de Privacidade</a></li>
              <li class="text-orange hover:text-white"><a href="">Políticas de Privacidade</a></li>
              <li class="text-orange hover:text-white"><a href="">Políticas de Privacidade</a></li>
            </ul>
          </div>

        </div>


      </div>

      <div class="grid grid-cols-1 justify-items-center">
        <p>Keroservice by. Todos os direitos reservados 2021</p>
      </div>

    </div>

  )
}

export default Rodape