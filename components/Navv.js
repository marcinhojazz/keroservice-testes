import Link from 'next/link'
import React from 'react'

function Navv() {
  return (
    <nav class="bg-gradient-to-t from-black to-black-dark px-4 py-2 flex flex-col lg:flex-row lg:items-center flex-shrink-0">
      <div class="flex justify-between items-center lg:mr-32">
        <span class="text-white text-xl hover:text-orange"><a href="#">KEROSERVICE</a></span>
        <button class="border border-white px-2 py-1 rounded text-white opacity-50 hover:opacity-75 lg:hidden" id="navbar-toggle">
          <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
          </svg>
        </button>
      </div>
      <div class="hidden lg:flex flex-grow" id="navbar-collapse">
        <ul class="flex flex-col mt-3 mb-1 lg:flex-row lg:mx-auto lg:mt-0 lg:mb-0">
          <li>
            <a href="/" class="block text-white font-semibold hover:text-orange py-2 lg:px-2 lg:mx-2 transition-colors duration-300">Início</a>
          </li>
          <li>
            <a href="/orcamento" class="block text-white font-semibold hover:text-orange py-2 lg:px-2 lg:mx-2 transition-colors duration-300">Orçamento</a>
          </li>
          <li>
            <a href="/profissionais" class="block text-white font-semibold hover:text-orange py-2 lg:px-2 lg:mx-2 transition-colors duration-300">Profissionais</a>
          </li>
          <li>
            <a href="#" class="block text-white font-semibold hover:text-orange py-2 lg:px-2 lg:mx-2 transition-colors duration-300">Sobre</a>
          </li>
          <li>
            <a href="#" class="block text-white font-semibold hover:text-orange py-2 lg:px-2 lg:mx-2 transition-colors duration-300">Blog</a>
          </li>
        </ul>
        <div class="flex my-3 lg:my-0">
          <Link href="/login">
            <button class="bg-transparent hover:bg-opacity-0 text-white hover:text-orange py-1 px-3 rounded border border-solid border-white hover:border-orange mr-2 transition-colors duration-300">
              Entrar
            </button>
          </Link>
          <Link href="/cadastro">
            <button class="text-3x1 bg-gradient-to-t from-orange to-orange-light hover:from-orange-dark hover:to-orange p-2 font-semibold text-gray-100 rounded">
              Cadastrar-se como profissional
            </button>
          </Link>
        </div>
      </div>
    </nav>
  )
}

export default Navv



// ---------------------

// import React from 'react'

// function Navv() {
//   return (
//     <nav class="bg-black px-4 py-2 flex flex-col lg:flex-row lg:items-center flex-shrink-0">
//       <div class="flex justify-between items-center lg:mr-32">
//         <span class="text-white text-xl hover:text-orange"><a href="#">KEROSERVICE</a></span>
//         <button class="border border-white px-2 py-1 rounded text-white opacity-50 hover:opacity-75 lg:hidden" id="navbar-toggle">
//           <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
//             <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
//           </svg>
//         </button>
//       </div>
//       <div class="hidden lg:flex flex-grow" id="navbar-collapse">
//         <ul class="flex flex-col mt-3 mb-1 lg:flex-row lg:mx-auto lg:mt-0 lg:mb-0">
//           <li>
//             <a href="#" class="block text-white font-semibold hover:text-orange py-2 lg:px-2 lg:mx-2 transition-colors duration-300">Início</a>
//           </li>
//           <li>
//             <a href="#" class="block text-white font-semibold hover:text-orange py-2 lg:px-2 lg:mx-2 transition-colors duration-300">Orçamento</a>
//           </li>
//           <li>
//             <a href="#" class="block text-white font-semibold hover:text-orange py-2 lg:px-2 lg:mx-2 transition-colors duration-300">Profissionais</a>
//           </li>
//           <li>
//             <a href="#" class="block text-white font-semibold hover:text-orange py-2 lg:px-2 lg:mx-2 transition-colors duration-300">Sobre</a>
//           </li>
//           <li>
//             <a href="#" class="block text-white font-semibold hover:text-orange py-2 lg:px-2 lg:mx-2 transition-colors duration-300">Blog</a>
//           </li>
//         </ul>
//         <div class="flex my-3 lg:my-0">
//           <button class="bg-transparent hover:bg-white text-white hover:text-gray-900 py-1 px-3 rounded border border-solid border-white hover:border-orange mr-2 transition-colors duration-300">
//             Entrar
//           </button>
//           <button class="bg-orange hover:bg-opacity-20 text-white py-1 px-3 rounded border border-solid border-indigo-600 hover:border-orange ring-orange transition-colors duration-300">
//             Cadastrar-se como profissional
//           </button>
//         </div>
//       </div>
//     </nav>
//   )
// }

// export default Navv
